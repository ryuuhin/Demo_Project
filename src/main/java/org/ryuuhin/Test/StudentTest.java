package org.ryuuhin.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @author ryuuhin
 * @date 2018/11/28 10:46
 */
public class StudentTest {

    public static void main(String[] args) {

        Student stu1 = new Student("小明", 20, "男");
        Student stu2 = new Student("小红", 21, "女");
        Student stu3 = new Student("小黄", 22, "男");
        Student stu4 = new Student("小李", 23, "男");
        Student stu5 = new Student("小青", 24, "女");


        Map<String, Integer> stuMap = new HashMap<>();
        stuMap.put("小兰",20);
        stuMap.put("小红",21);
        stuMap.put("小黄",22);
        stuMap.put("小明",30);
        stuMap.put("小李",23);

        Set<String> s= stuMap.keySet();
        Iterator<String> name =  s.iterator();
        while (name.hasNext())
        {
            String sname = name.next();
            System.out.println(sname);
        }

        for (Object o : stuMap.entrySet()) {
            Map.Entry<String, Integer> e = (Map.Entry) o;

            System.out.println(e.getKey() + " : " + e.getValue().toString());
        }
    }
}
