package org.ryuuhin.Test;

/**
 * @author ryuuhin
 * @date 2018/11/29 9:58
 */
public class Base {

    static {
        System.out.println("base static");
    }

    public Base() {
        System.out.println("base constructor");
    }
}
