package org.ryuuhin.Test;

import org.ryuuhin.ap.common.utils.ConvertUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;

public class Test {
    public static LocalDate SINGLE_WEEK_DATE = null;
    public static void main(String[] args) throws Exception {

        SINGLE_WEEK_DATE = ConvertUtil.strToLocalDate("2018-11-07");
        System.out.println("减1天: " + SINGLE_WEEK_DATE.minusDays(1));
        System.out.println("减1周: " + SINGLE_WEEK_DATE.minusWeeks(1));


        LocalDate startDate = SINGLE_WEEK_DATE.with(DayOfWeek.MONDAY);
        String dateValue =  String.valueOf((LocalDate.now().toEpochDay() - startDate.toEpochDay()) % 14 + 1);
        System.out.println(dateValue);

        LocalDate date = LocalDate.now();
        System.out.println(date.toEpochDay());


//        byteOutStream();
//        System.out.println(toStringHex("A1CC"));
//        System.out.println(hexStringToString("A1CC"));
    }


    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }
    //转换十六进制编码为字符串
    public static String toStringHex(String s) {
        if ("0x".equals(s.substring(0, 2))) {
            s = s.substring(2);
        }
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(
                        i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            s = new String(baKeyword, "utf-8");//UTF-16le:Not
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return s;
    }

    public static String hexStringToString(String s) {
        if (s == null || s.equals("")) {
            return null;
        }
        s = s.replace(" ", "");
        byte[] baKeyword = new byte[s.length() / 2];
        for (int i = 0; i < baKeyword.length; i++) {
            try {
                baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
            s = new String(baKeyword, "UTF-8");
            new String();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        return s;
    }

    public static void byteOutStream() throws Exception {

        //1:使用File类创建一个要操作的文件路径
        File file = new File("C:/dev/test.txt");
        if(!file.getParentFile().exists()){ //如果文件的目录不存在
            file.getParentFile().mkdirs(); //创建目录

        }

        //2: 实例化OutputString 对象
        OutputStream output = new FileOutputStream(file);

        //3: 准备好实现内容的输出

        String msg = toStringHex("A1CC");
        //将字符串变为字节数组
        byte data[] = msg.getBytes();
        output.write(data);
        //4: 资源操作的最后必须关闭
        output.close();

    }
}
