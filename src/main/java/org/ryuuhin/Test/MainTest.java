package org.ryuuhin.Test;

/**
 * @author ryuuhin
 * @date 2018/11/29 10:26
 */
public class MainTest {

    static int value = 33;

    public static void main(String[] args) {
   //     new MainTest().printValue();

        String a = "hello2";
        final String b = "hello";
        String d = "hello";
        String c = b + 2;
        String e = d + 2;
        System.out.println((a == c));
        System.out.println((a == e));


    }

    public void printValue() {
        int value = 3;
        System.out.println(value);
        System.out.println(this.value);
    }


//    public static void main(String[] args){
//        MainTest testFinal = new MainTest();
//        StringBuffer buffer = new StringBuffer("hello");
//        testFinal.changeValue(buffer);
//        System.out.println(buffer);
//
//    }

    public void changeValue(final StringBuffer buffer){
        //final修饰引用类型的参数，不能再让其指向其他对象，但是对其所指向的内容是可以更改的。
        //buffer = new StringBuffer("hi");
        buffer.append("world");


    //    System.out.println(buffer);
    }
}
