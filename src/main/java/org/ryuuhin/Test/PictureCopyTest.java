package org.ryuuhin.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Java IO 创建文件解决文件名重复问题
 */
public class PictureCopyTest {


    public static void main(String[] args) throws IOException {
        //源文件
        File sourceFile = new File("C:\\Users\\bin.liu\\Desktop\\jir42.png");
//文件的完整名称
        String fileName = sourceFile.getName();
//文件名
        String name = fileName.substring(0, fileName.indexOf("."));
//文件后缀
        String suffix = fileName.substring(fileName.lastIndexOf("."));
//目标文件
        File deseFile = new File("C:\\liubin\\test\\picture" + File.separator + fileName);

        int i = 1;
//若文件存在重命名
        while (deseFile.exists()) {
            String newFileName = name + "(" + i + ")" + suffix;
            String parentPath = deseFile.getParent();
            deseFile = new File(parentPath + File.separator + newFileName);
            i++;
        }

        deseFile.createNewFile();//新建文件
        FileInputStream fin = new FileInputStream(sourceFile);
        FileOutputStream fout = new FileOutputStream(deseFile);
        byte[] data = new byte[512];
        int rs = -1;
        while ((rs = fin.read(data)) > 0) {
            fout.write(data, 0, rs);
        }
        fout.close();
        fin.close();

    }
}
