package org.ryuuhin.Test;

import org.ryuuhin.ap.common.utils.SqlUtil;

import java.io.*;
import java.nio.charset.StandardCharsets;

/**
 * @author ryuuhin
 * @date 2018/11/28 15:14
 */
public class SqlTest {



    public String getsql() {
        String sql = SqlUtil.getSql("/sql/diGuardTaskQuery.sql", true);

        return sql;
    }

    public String getTxt() {
        StringBuilder out = new StringBuilder();
        try {
            InputStream is = SqlTest.class.getResourceAsStream("/txt/aaa.txt");
            InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);

            String line;
            while ((line = br.readLine()) != null){
                out.append(line).append("\n\r");
            }

            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return out.toString();
    }


    public static void main(String[] args) {
        SqlTest st = new SqlTest();
     //  System.out.println(st.getsql());
        System.out.println(st.getTxt());
    }
}
