package org.ryuuhin.Test;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ResourceHelper {

    public static BufferedInputStream getResourceInput(String ResourceName){
        try {
            return new BufferedInputStream(new FileInputStream(ResourceName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
