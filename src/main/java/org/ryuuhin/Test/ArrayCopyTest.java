package org.ryuuhin.Test;

/**
 * java语言复制数组的四种方法
 * 效率：System.arraycopy > clone > Arrays.copyOf > for循环
 */
public class ArrayCopyTest {


    public static void main(String[] args) {



        //1、System.arraycopy的用法：
/*        int[] a1 = {1,2,3,4,5};
        int[] a2 = {10,11,12,13,14,15,16,17,18,19};
        System.arraycopy(a1, 1, a2,5, 4 );
        System.out.println("copy后结果：");
        for (int i = 0; i < a2.length; i++) {
            System.out.println(a2[i]);
        }*/

        //2、clone 的用法：
        //被克隆的类要实现Cloneable接口
     /*   class Cat implements Cloneable
        {
            private String name;
            private int age;
            public Cat(String name,int age)
            {
                this.name=name;
                this.age=age;
            }
            //重写clone()方法
            protected Object clone()throws CloneNotSupportedException{
                return super.clone() ;
            }
        }
        public class Clone {
            public static void main(String[] args) throws CloneNotSupportedException {
                Cat cat1=new Cat("xiaohua",3);
                System.out.println(cat1);
                //调用clone方法
                Cat cat2=(Cat)cat1.clone();
                System.out.println(cat2);
            }
        }*/

/*        //复制引用：是指将某个对象的地址复制，所以复制后的对象副本的地址和源对象相同，这样，当改变副本的某个值后，源对象值也被改变；
        //复制对象：是将源对象整个复制，对象副本和源对象的地址并不相同，当改变副本的某个值后，源对象值不会改变
        Cat cat1=new Cat("xiaohua",3);//源对象
        System.out.println("源对象地址"+cat1);
        //调用clone方法，复制对象
        Cat cat2=(Cat)cat1.clone();
        Cat cat3=(Cat)cat1;//复制引用
        System.out.println("复制对象地址："+cat2);
        System.out.println("复制引用地址："+cat3);
        //可以看出，复制引用的对象和源对象地址相同，复制对象和源对象地址不同*/


    }
}
