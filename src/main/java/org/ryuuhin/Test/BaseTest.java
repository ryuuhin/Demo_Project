package org.ryuuhin.Test;

/**
 * @author ryuuhin
 * @date 2018/11/29 10:00
 */
public class BaseTest extends Base{
    static {
        System.out.println("BaseTest static");
    }

    public BaseTest() {
        System.out.println("BaseTest constructor");
    }

    public static void main(String[] args) {
        new BaseTest();
    }
}
