package org.ryuuhin.Test;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.Collection;
import java.util.Set;

/**
 * @author liubin
 * @Date 2018/11/16
 *
 * Multimap 提供了一个方便地把一个键对应到多个值的数据结构。
 * 理解Multimap:”键-单个值映射”的集合(例如：a -> 1 a -> 2 a ->4 b -> 3 c -> 5)
 * 特点：不会有任何键映射到空集合：一个键要么至少到一个值，要么根本就不在Multimap中。
 */
public class MultimapTest {

    class Student{
        String name;
        int age;
    }

    private static final String CLASS_NAME1 = "一年级";
    private static final String CLASS_NAME2 = "二年级";

    private Multimap<String, Student> multimap = ArrayListMultimap.create();

    public void testMultiMap(){

        for (int i = 0; i < 5; i++){
            Student student = new Student();
            student.name = "张小明" + i;
            student.age = 7;
            multimap.put(CLASS_NAME1, student);
        }

        for (int i = 0; i < 6; i++){
            Student student = new Student();
            student.name = "王小五" + i;
            student.age = 8;
            multimap.put(CLASS_NAME2, student);
        }

        for (Student stu : multimap.get(CLASS_NAME1)){
            System.out.println("一年级学生name：" + stu.name + ",age: " + stu.age);
        }

        for (Student stu : multimap.get(CLASS_NAME2)){
            System.out.println("二年级学生name：" + stu.name + ",age: " + stu.age);
        }

        //判断键是否存在
        if (multimap.containsKey(CLASS_NAME1)) {
            System.out.println("键值包含："+ CLASS_NAME1);
            Collection<Student> students = multimap.get(CLASS_NAME1);
            System.out.println("-------------------------");
            for (Student stu : students){
                System.out.println(stu);
                System.out.println("一年级学生name：" + stu.name + ",age: " + stu.age);
            }
            System.out.println("-------------------------");
        }

        //”键-单个值映射”的个数
        System.out.println(multimap.size());

         Set<String> strClass = multimap.keySet();
        System.out.println(strClass);



        //不同键的个数
        System.out.println(multimap.keySet().size());
    }

    public static void main(String[] args) {
        MultimapTest multimapTest = new MultimapTest();
        multimapTest.testMultiMap();
    }

}
