package org.ryuuhin.Test.taskTest;

/**
 * @auther: bin.liu
 * @Date: 2019/1/18 16:29
 * @Description: spring的两种定时器：TimerTask 和 Quartz
 * Quartz可以通过cron表达式精确到特定时间执行，而TimerTask不能。Quartz拥有TimerTask所有的功能，而TimerTask则没有。
 */
public class quartz {

/*    精确度和功能
    Quartz可以通过cron表达式精确到特定时间执行，而TimerTask不能。
    Quartz拥有SpringTask所有的功能，而TimerTask则没有。

    任务类的数量
    Quartz每次执行都创建一个新的任务类对象。
    SpringTask则每次使用同一个任务类对象。

    对异常的处理

    Quartz的某次执行任务过程中抛出异常，不影响下一次任务的执行，当下一次执行时间到来时，定时器会再次执行任务。
    SpringTask不同，一旦某个任务在执行过程中抛出异常，则整个定时器生命周期就结束，以后永远不会再执行定时器任务。
    总结:还是第三方考虑的周全，东西比较多，加上Quartz配置简单，maven也就是多加一个jar包，所以一般情况下还是使用Quartz了。*/




//     应该写在配置文件中
/*        <bean id="job" class=" xx.xx.xx.Job" />
    <bean id="cronTask" class="org.springframework.scheduling.quartz.MethodInvokingJobDetailFactoryBean">
        <property name="targetObject" ref="job" />
        <property name="targetMethod" value="runWork" />
        <!-- false表示job不会并发执行，默认为true-->
        <property name="concurrent" value="false" />
    </bean>
    targetObject属性指定的任务类，有多种方式实现。
            1、可以用@Component注解在类上面标注，这样就不用定义<bean id="job" ... />这些东西了。
            2、可以按上面的写法来配置。
            3、直接使用下面的写法。
    <property name="targetObject">
        <bean class="xx.xx.xx.Job" />
    </property>
    接下来配置触发器
            <bean id="doWork" class="org.springframework.scheduling.quartz.CronTriggerBean">
        <property name="jobDetail" ref="cronTask" />
        <!—每天凌晨0点1分执行-->
        <property name="cronExpression" value="0 01 00 * * ?" />
    </bean>
    最后配置调度工厂
            <bean class="org.springframework.scheduling.quartz.SchedulerFactoryBean">
        <property name="triggers">
            <list>
                <ref local="doWork"/>
            </list>
        </property>
    </bean>*/
}
