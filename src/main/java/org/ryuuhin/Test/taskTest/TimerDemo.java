package org.ryuuhin.Test.taskTest;

import java.util.Timer;

/**
 * @auther: bin.liu
 * @Date: 2019/1/18 11:07
 * @Description: 在jdk自带的库中,有两种技术可以实现定时任务。一种是使用Timer,另外一个则是ScheduledThreadPoolExecutor。
 * 使用Timer,Timer是单线程的,如果一次执行多个定时任务，会导致某些任务被其他任务所阻塞
 */
public class TimerDemo {

    public static void main(String[] args) {
        //创建定时器对象
        Timer t=new Timer();
        //在3秒后执行MyTask类中的run方法,后面每10秒跑一次
        t.schedule(new MyTask(),3000,10000);
    }
}
