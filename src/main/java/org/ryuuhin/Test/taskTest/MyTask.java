package org.ryuuhin.Test.taskTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

/**
 * @auther: bin.liu
 * @Date: 2019/1/18 11:02
 * @Description:
 */
public class MyTask extends TimerTask {

    @Override
    public void run(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String now = sdf.format(new Date());
        System.out.println(now + "    hello world");
    }
}
