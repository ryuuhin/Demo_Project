package org.ryuuhin.ap.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.ryuuhin.ap.entity.SysRoleEntity;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserDomain extends BaseDomain{

    /**
     * 用户名
     */
    private String username;

    /**
     * 账户
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 状态
     */
    private int status;

    private Long businessUserId;

    /**
     * 角色列表
     */
    private List<SysRoleEntity> roleList;
}
