package org.ryuuhin.ap.repository;

import org.ryuuhin.ap.entity.SysRoleEntity;
import org.ryuuhin.ap.entity.SysUserRoleEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SysUserRoleRepository extends IJpaRepository<SysUserRoleEntity, Long>{

    /**
     * 根据当前用户id，查询用户角色
     * @param userId
     * @return
     */
    @Query("select sr from SysRoleEntity sr,SysUserRoleEntity sur where sr.id = sur.roleId and sur.userId = :userId and sr.status = 1")
    List<SysRoleEntity> findRolesByUserId(@Param(value = "userId") Long userId);
}
