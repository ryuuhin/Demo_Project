package org.ryuuhin.ap.repository;

import org.ryuuhin.ap.entity.SysUserEntity;

public interface SysUserRepository extends IJpaRepository<SysUserEntity, Long> {

    /**
     * 根据账户查询
     * @param account
     * @return
     */
    SysUserEntity findFirstByAccount(String account);

    SysUserEntity findFirstById(Long id);

    SysUserEntity findFirstByAccountAndStatus(String account, int status);
}
