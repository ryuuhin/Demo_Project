package org.ryuuhin.ap.repository;

import org.ryuuhin.ap.entity.SysRoleEntity;

public interface SysRoleRepository extends IJpaRepository<SysRoleEntity, Long>{

    SysRoleEntity findFirstByRoleNameEquals(String roleName);
}
