package org.ryuuhin.ap.service.impl;

import com.google.common.collect.Lists;
import org.ryuuhin.ap.common.utils.EntityUtil;
import org.ryuuhin.ap.domain.SysUserDomain;
import org.ryuuhin.ap.entity.SysRoleEntity;
import org.ryuuhin.ap.entity.SysUserEntity;
import org.ryuuhin.ap.repository.SysUserRepository;
import org.ryuuhin.ap.repository.SysUserRoleRepository;
import org.ryuuhin.ap.service.BaseService;
import org.ryuuhin.ap.service.ISysUserService;
import org.ryuuhin.ap.vo.SysUserVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SysUserServiceImpl extends BaseService<SysUserEntity, Long> implements ISysUserService {

    @Autowired
    private SysUserRepository sysUserRepository;

    @Autowired
    private SysUserRoleRepository sysUserRoleRepository;

    @Override
    public CrudRepository<SysUserEntity, Long> getDao() {
        return sysUserRepository;
    }

    /**
     * 查询用户列表
     */
    @Override
    public List<SysUserEntity> findAll(){
        return sysUserRepository.findAll();
    }

    @Override
    public SysUserEntity findFirstById(Long id){
        return sysUserRepository.findFirstById(id);
    }

    /**
     * 新增用户
     */
    @Transactional
    @Override
    public void addUser(SysUserVo sysUserVo){
        SysUserEntity sysUserEntity = new SysUserEntity();
        BeanUtils.copyProperties(sysUserVo, sysUserEntity);
        sysUserEntity.setCreateTime(LocalDateTime.now());
        sysUserEntity.setDeletedStatus(false);
        sysUserRepository.save(sysUserEntity);
    }

    /**
     * 更新用户
     */
    @Transactional
    @Override
    public void updateUser(SysUserVo sysUserVo, SysUserEntity sysUserEntity){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        sysUserVo.setPassword(encoder.encode(sysUserVo.getPassword().toLowerCase().trim()));
        BeanUtils.copyProperties(sysUserVo, sysUserEntity, EntityUtil.getNullPropertyNames(sysUserVo));
        sysUserEntity.setUpdateTime(LocalDateTime.now());
        sysUserRepository.save(sysUserEntity);
    }


    @Override
    public SysUserDomain findFirstByUsername(String username){
        SysUserDomain sysUserDomain = new SysUserDomain();
        SysUserEntity sysUserEntity = sysUserRepository.findFirstByAccountAndStatus(username, 1);
        if (Objects.isNull(sysUserEntity)) {
            return null;
        }
        sysUserDomain.setId(sysUserEntity.getId());
        sysUserDomain.setUsername(sysUserEntity.getUsername());
        sysUserDomain.setPassword(sysUserEntity.getPassword());
        sysUserDomain.setAccount(sysUserEntity.getAccount());
        sysUserDomain.setEmail(sysUserEntity.getEmail());
        sysUserDomain.setMobile(sysUserEntity.getMobile());
        sysUserDomain.setBusinessUserId(sysUserEntity.getBusinessUserId());
        sysUserDomain.setStatus(sysUserEntity.getStatus());
        List<SysRoleEntity> sysRoleEntities = sysUserRoleRepository.findRolesByUserId(sysUserEntity.getId());
        sysUserDomain.setRoleList(Optional.ofNullable(sysRoleEntities).orElse(new ArrayList()));
        logger.debug("当前登录的用户信息为：{}", sysUserDomain.toString());
        return sysUserDomain;
    }
}
