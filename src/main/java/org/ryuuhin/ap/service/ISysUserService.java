package org.ryuuhin.ap.service;

import org.ryuuhin.ap.domain.SysUserDomain;
import org.ryuuhin.ap.entity.SysUserEntity;
import org.ryuuhin.ap.vo.SysUserVo;

import java.util.List;

public interface ISysUserService extends IService<SysUserEntity, Long>{

    /**
     * 根据用户名，查询系统用户
     *
     * @param username 用户
     * @return
     */
    SysUserDomain findFirstByUsername(String username);

    SysUserEntity findFirstById(Long id);
    /**
     * 新增用户
     */
    void addUser(SysUserVo sysUserVo);

    /**
     * 查询用户列表
     */
    List<SysUserEntity> findAll();

    /**
     * 更新用户
     */
    void updateUser(SysUserVo sysUserVo, SysUserEntity sysUserEntity);
}
