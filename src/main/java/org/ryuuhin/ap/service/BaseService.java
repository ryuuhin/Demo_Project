package org.ryuuhin.ap.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import java.io.Serializable;
import java.util.List;

@SuppressWarnings("unchecked")
public abstract class BaseService<T, ID extends Serializable> implements IService<T, ID>{

    public Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public abstract CrudRepository<T, ID> getDao();

    /**
     *  添加
     * @param element
     * @return
     */
    @Override
    public T save(T element){
        return getDao().save(element);
    }

    /**
     * 删除
     * @param element
     */
    @Override
    public void delete(T element){
        getDao().delete(element);
    }

    /**
     * 根据id删除
     * @param id
     */
    @Override
    public void delete(ID id){
        getDao().delete(id);
    }

    /**
     * 查询所有对象
     * @return
     */
    @Override
    public List<T> findALL(){
        return (List<T>) getDao().findAll();
    }

    /**
     * 根据id查询对象
     * @return
     */
    @Override
    public T findOne(ID id){
        return getDao().findOne(id);
    }

    /**
     * 返回是否存在具有给定标识的实体。
     * @param id
     * @return
     */
    @Override
    public boolean exists(ID id){
        return  getDao().exists(id);
    }
}
