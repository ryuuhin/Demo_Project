package org.ryuuhin.ap.service;

import org.springframework.data.repository.CrudRepository;

import java.io.Serializable;
import java.util.List;

public interface IService<T,ID extends Serializable> {

    CrudRepository<T, ID> getDao();

    /**
     *  添加
     * @param element
     * @return
     */
    T save(T element);

    /**
     * 删除
     * @param element
     */
    void delete(T element);

    /**
     * 根据id删除
     * @param id
     */
    void delete(ID id);

    /**
     * 查询所有对象
     * @return
     */
    List<T> findALL();

    /**
     * 根据id查询对象
     * @return
     */
    T findOne(ID id);

    /**
     * 返回是否存在具有给定标识的实体。
     * @param id
     * @return
     */
    boolean exists(ID id);
}
