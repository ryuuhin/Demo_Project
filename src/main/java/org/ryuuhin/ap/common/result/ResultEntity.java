package org.ryuuhin.ap.common.result;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class ResultEntity<T> implements Serializable {

    /**
     * 返回状态码
     */
    @ApiModelProperty(value = "返回状态码(200成功||500失败||401未登录||403没有权限)",example = "200")
    private int code;
    /**
     * 返回提示
     */
    @ApiModelProperty(value = "返回提示",example = "成功")
    private String msg;
    /**
     * 请求结果
     */
    @ApiModelProperty(value = "返回结果",example = "{}")
    private T data;

    public ResultEntity(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultEntity(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultEntity() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
