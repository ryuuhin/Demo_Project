package org.ryuuhin.ap.common.result;

import org.ryuuhin.ap.common.result.ResultEntity;
import org.springframework.http.HttpStatus;

public class ResultUtil {

    public static <T> ResultEntity<T> error(T data) {
        return error("失败", data);
    }

    public static ResultEntity error(String message) {
        return error(message, null);
    }

    public static ResultEntity error() {
        return error("失败", null);
    }

    public static ResultEntity success() {
        return success("成功", null);
    }

    public static <T> ResultEntity<T> success(T data) {
        return success("成功", data);
    }

    public static <T> ResultEntity general(int code, String message, T data){
        ResultEntity<T> resultEntity = new ResultEntity<T>();
        resultEntity.setCode(code);
        resultEntity.setMsg(message);
        resultEntity.setData(data);
        return resultEntity;
    }

    public static ResultEntity noLogin(int code, String message) {
        return general(code,message,null);
    }

    public static <T> ResultEntity<T> error(String message, T data) {
        return general(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, data);
    }


    public static <T> ResultEntity<T> success(String message, T data){
        return general(HttpStatus.OK.value(),message, data);
    }
}
