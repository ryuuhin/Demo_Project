package org.ryuuhin.ap.common.utils;

import org.springframework.util.StringUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 * 数据转换
 *
 */
public class ConvertUtil {

    private static final String YHD = "yyyy-MM-dd";

    private static final String YHDHMS = "yyyy-MM-dd HH:mm:ss";

    private static final String HMS = "HH:mm:ss";


    public static DateTimeFormatter DF_YMD_HMS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static DateTimeFormatter DF_YMD = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static DateTimeFormatter DF_HMS = DateTimeFormatter.ofPattern("HH:mm:ss");

    /**
     * String类型的日期"yyyy-MM-dd"转成LocalDate类型的日期"yyyy-MM-dd"
     *
     * @param dateStr
     * @return
     */
    public static LocalDate strToLocalDate(String dateStr) {
        LocalDate localDate = null;
        if (!StringUtils.isEmpty(dateStr)) {
            try {
                localDate = LocalDate.parse(dateStr, DateTimeFormatter.ofPattern(YHD));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return localDate;
    }

    /**
     * String类型的日期"yyyy-MM-dd HH:mm:ss"转成LocalDate类型的日期"yyyy-MM-dd HH:mm:ss"
     *
     * @param dateStr
     * @return
     */
    public static LocalDateTime strToLocalDateTime(String dateStr) {
        LocalDateTime localDateTime = null;
        if (!StringUtils.isEmpty(dateStr)) {
            try {
                localDateTime = LocalDateTime.parse(dateStr, DateTimeFormatter.ofPattern(YHDHMS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return localDateTime;
    }

    /**
     * String类型的时间"HH:mm:ss"转换成LocalTime"HH:mm:ss"
     *
     * @param timeStr
     * @return
     */
    public static LocalTime strToLocalTime(String timeStr) {
        LocalTime localTime = null;
        if (!StringUtils.isEmpty(timeStr)) {
            try {
                localTime = LocalTime.parse(timeStr, DateTimeFormatter.ofPattern(HMS));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return localTime;
    }

    /**
     * String类型转成Long类型
     */
    public static Long strToLong(String str) {
        Long value = null;
        if (!StringUtils.isEmpty(str)) {
            try {
                value = Long.valueOf(str);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        return value;
    }

    /**
     * Long 默认处理
     *
     * @param conver
     * @return
     */
    public static Long nullToLong(Long conver) {
        Long id = 0L;
        if (Objects.isNull(conver)) {
            return id;
        } else {
            id = conver;
        }
        return id;
    }
}
