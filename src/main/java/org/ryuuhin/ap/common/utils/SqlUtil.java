package org.ryuuhin.ap.common.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ryuuhin
 * created date 2018/8/24
 **/
public class SqlUtil {

    private static Map<String, String> cache = new HashMap();

    public static String getSql(String path, boolean isCache) {
        return cache.containsKey(path) ? cache.get(path) : recoverySql(path, isCache);
    }

    public static void setSql(String path, String sql) {
        cache.put(path, sql);
    }

    private static String recoverySql(String path, boolean isCache) {
        try {
            InputStream is = SqlUtil.class.getResourceAsStream(path);
            StringBuilder out = new StringBuilder();
            String line;
            boolean first = true;

            for (BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8)); (line = reader.readLine()) != null; out.append(line).append("\r\n")) {
                if (first) {
                    int index = line.indexOf("SELECT");
                    if (index < 0) {
                        index = line.indexOf("UPDATE");
                    }
                    if (index < 0) {
                        index = line.indexOf("DELETE");
                    }
                    if (index < 0) {
                        index = line.indexOf("INSERT");
                    }
                    if (index < 0) {
                        index = line.indexOf("MERGE");
                    }
                    line = line.toUpperCase().substring(index);
                    first = false;
                }
            }

            is.close();
            if (isCache) {
                cache.put(path, out.toString());
            }

            return out.toString();
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("读取SQL文件失败！文件路径：" + path);
        }
    }

//    public static String setParameters(String sql, Map<String, String> paramMap) {
//        String key;
//        for (Iterator var2 = paramMap.keySet().iterator(); var2.hasNext(); sql = sql.replace(":" + key, "'" + (String) paramMap.get(key) + "'")) {
//            key = (String) var2.next();
//        }
//
//        return sql;
//    }

}
