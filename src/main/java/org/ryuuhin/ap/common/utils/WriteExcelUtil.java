package org.ryuuhin.ap.common.utils;

import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * @author ryuuhin
 * @date 2018/7/10  下午3:26
 */
public class WriteExcelUtil {
    /**
     * @param response 下载请求的response
     */
    public static void writeExce(HttpServletResponse response,String fileName,Workbook workbook) {
        try (OutputStream out = response.getOutputStream()){
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "attachment; filename="
                    + URLEncoder.encode(fileName, "UTF-8"));
            workbook.write(out);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
