package org.ryuuhin.ap.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SysUserVo {

    @ApiModelProperty("人员")
//    @NotNull(message = "人员不能为空", groups = {AddGroup.class})
    private Long businessUserId;

    @ApiModelProperty("用户名")
//    @NotBlank(message = "用户名不能为空", groups = {AddGroup.class})
    private String username;

    @ApiModelProperty("账号")
//    @NotBlank(message = "账号不能为空", groups = {AddGroup.class})
    private String account;

    @ApiModelProperty("密码")
//    @NotBlank(message = "密码不能为空", groups = {AddGroup.class})
//    @Size(min = 4, message = "密码至少4位", groups = {AddGroup.class})
    private String password;

    @ApiModelProperty("角色")
//    @NotNull(message = "角色不能为空", groups = {AddGroup.class})
    private Long roleId;

    @ApiModelProperty("状态(ENABLE_STATUS)")
//    @NotNull(message = "状态不能为空", groups = {AddGroup.class})
    private int status;
}
