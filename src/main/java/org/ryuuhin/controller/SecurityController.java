package org.ryuuhin.controller;

import org.ryuuhin.ap.common.result.ResultEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller  // 注意不要是RestController
@RequestMapping(value = "/")
public class SecurityController {


    @GetMapping(value = "/login")
    public String userLogin()
    {
        return "login";
    }

    @GetMapping("/login-error")
    public String loginError(Model model)
    {
        ResultEntity rst = new ResultEntity(500, "失败", "登录失败");
        model.addAttribute("rst", rst);
        return "login-error";
    }

    @GetMapping("/layout/nav-menu")
    public String index(Model model) {
        ResultEntity rst = new ResultEntity(200, "成功", "登录信息，只对管理员显示");
        model.addAttribute("rst", rst);
        return "layout/nav-menu";
    }

    @GetMapping("/logout")
    public String logout(HttpSession session){
        return "login";
    }
}
