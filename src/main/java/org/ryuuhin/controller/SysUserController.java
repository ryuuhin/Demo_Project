package org.ryuuhin.controller;

import io.swagger.annotations.ApiOperation;
import org.ryuuhin.ap.common.result.ResultEntity;
import org.ryuuhin.ap.common.result.ResultUtil;
import org.ryuuhin.ap.entity.SysUserEntity;
import org.ryuuhin.ap.service.ISysUserService;
import org.ryuuhin.ap.vo.SysUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/system/user")
public class SysUserController extends BaseController{

    @Autowired
    private ISysUserService iSysUserService;

    @ApiOperation(value = "查询用户列表")
    @GetMapping
    public ResultEntity findAll() {
        List<SysUserEntity> sysUserEntities = iSysUserService.findAll();
        return ResultUtil.success(sysUserEntities);
    }

    @ApiOperation(value = "添加用户")
    @PostMapping
    public ResultEntity addUser(SysUserVo sysUserVo){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        sysUserVo.setPassword(encoder.encode(sysUserVo.getPassword().toLowerCase().trim()));
        iSysUserService.addUser(sysUserVo);
        return ResultUtil.success();
    }

    @ApiOperation(value = "更新用户")
    @PutMapping(value = "/{id}")
    public ResultEntity updateUser(@PathVariable(value = "id") Long id, @RequestBody SysUserVo sysUserVo) {
        SysUserEntity sysUserEntity = iSysUserService.findFirstById(id);

        iSysUserService.updateUser(sysUserVo,sysUserEntity);
        return ResultUtil.success();
    }

}
