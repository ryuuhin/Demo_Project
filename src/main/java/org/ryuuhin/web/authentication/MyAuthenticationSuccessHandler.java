package org.ryuuhin.web.authentication;

import org.ryuuhin.ap.common.result.ResultUtil;
import org.ryuuhin.ap.common.utils.GsonUtil;
import org.ryuuhin.web.domian.MyUserDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 * 登录成功后的操作
 */
@Component("MyAuthenticationSuccessHandler")
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(MyAuthenticationSuccessHandler.class);

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                 HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        Map<String, Object> map = new HashMap<>(5);
        if (authentication != null) {
            response.setStatus(HttpStatus.OK.value());
            Object principal = authentication.getPrincipal();
            if (principal instanceof MyUserDetails) {
                MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
                map.put("username", userDetails.getUsername());
                map.put("avatar", userDetails.getAvatar());
                map.put("account",userDetails.getAccount());
                map.put("id", Optional.ofNullable(userDetails.getId()).orElse(0L));
            }
        }

        response.getWriter().write(GsonUtil.JsonString(ResultUtil.success(map)));
        logger.debug("登录成功{}",GsonUtil.JsonString(ResultUtil.success(map)));
    }
}
