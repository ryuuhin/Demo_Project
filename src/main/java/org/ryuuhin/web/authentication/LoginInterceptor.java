package org.ryuuhin.web.authentication;

import org.ryuuhin.ap.entity.SysUserEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 登录验证拦截
 */
@Controller
@Component
public class LoginInterceptor extends HandlerInterceptorAdapter {

    private Logger logger = LoggerFactory.getLogger(LoginInterceptor.class);


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception{

        logger.info("------preHandle------");

        String basePath = request.getContextPath();
        String path = request.getRequestURI();

        // 是否进行登录拦截
        if(!doLoginInterceptor(path,basePath)) {
            // 不拦截
            return  true;
        }

        //如果登录了，会把用户信息存进session
        HttpSession session = request.getSession();
        List<SysUserEntity> userEntities = (List<SysUserEntity>) session.getAttribute("userList");

        if (userEntities == null) {

            String requestType = request.getHeader("X-Requested-With");
            if (requestType != null && requestType.equals("XMLHttpRequest")) {
                response.setHeader("sessionstatus","timeout");
                response.getWriter().print("LoginTimeout");
                return false;
            } else {
                logger.info("尚未登录，跳转到登录页面");
                response.sendRedirect(request.getContextPath()+ "/login");
            }
            return false;
        }
        logger.info("用户已登录");
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        // TODO Auto-generated method stub

    }

    /**
     * 是否进行登录拦截
     * @param path
     * @param basePath
     * @return
     */
    private boolean doLoginInterceptor(String path, String basePath){
        path = path.substring(basePath.length());
        Set<String> notLoginPaths = new HashSet<>();

        // 设置不进行登录拦截的路径：登录注册和验证码
        notLoginPaths.add("/");
        notLoginPaths.add("/login");
        notLoginPaths.add("/index");

        if(notLoginPaths.contains(path)) {
            return false;
        }
        return true;
    }
}
