package org.ryuuhin.web.authentication;

import org.ryuuhin.ap.common.result.ResultUtil;
import org.ryuuhin.ap.common.utils.GsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 注销成功
 */
@Component("myAuthenticationLogoutSuccessHandler")
public class MyAuthenticationLogoutSuccessHandler implements LogoutSuccessHandler {

    private Logger logger = LoggerFactory.getLogger(MyAuthenticationLogoutSuccessHandler.class);

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                         Authentication authentication) throws IOException, ServletException{
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.setStatus(HttpStatus.OK.value());
        if (authentication != null) {
            response.getWriter().write(GsonUtil.JsonString(ResultUtil.success("注销成功")));
        } else {
            response.getWriter().write(GsonUtil.JsonString(ResultUtil.success("您已经注销")));
        }

        logger.debug("注销成功{}",GsonUtil.JsonString(authentication));
    }
}
