package org.ryuuhin.web.service;

import org.ryuuhin.ap.domain.SysUserDomain;
import org.ryuuhin.ap.service.ISysUserService;
import org.ryuuhin.web.domian.MyUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * 自定义MyUserDetailsService需实现UserDetailsService接口。可直接返回给springSecurity使用。
 * @author liubin
 *
 */
@Service("MyUserDetailsService")
public class MyUserDetailsService implements UserDetailsService {

    @Autowired
    private ISysUserService sysUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{

        SysUserDomain sysUserDomain = sysUserService.findFirstByUsername(username);
        if (sysUserDomain == null) {
            throw new UsernameNotFoundException(String.format("用户%s不存在",username));
        }
        System.out.println(sysUserDomain.toString());
        return new MyUserDetails(sysUserDomain);
    }
}
