package org.ryuuhin.web.config;

import org.ryuuhin.web.authentication.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * springMVC配置
 * @author liubin
 */
public class WebMvcConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private LoginInterceptor loginInterceptor;

    /**
     * 不需要登录拦截的url：登录注册和验证码
     */
    final String[] notLoginIntercepPaths = {"/login"};

    public void addInterceptors(InterceptorRegistry registry) {
        //登录拦截器,除了login其他都拦截
        registry.addInterceptor(loginInterceptor).addPathPatterns("/**").excludePathPatterns(notLoginIntercepPaths);
        super.addInterceptors(registry);
    }

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public InternalResourceViewResolver viewResolver(){
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/templates/");
        resolver.setSuffix(".html");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }


    /**
     * 注册访问登录转向login.html页面
     * @param registry
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/layout/nav-menu").setViewName("layout/nav-menu");
        super.addViewControllers(registry);
    }
}
