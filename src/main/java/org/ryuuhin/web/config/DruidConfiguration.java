package org.ryuuhin.web.config;

import com.alibaba.druid.pool.DruidDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * 配置druid需要的配置类，引入application文件中以spring.datasource开头的信息
 * 因此需要在application文件中配置相关信息。
 * @author geniusj
 * @email 975473185@qq.com
 * @date 2018/7/2  下午5:23
 */
@Configuration
public class DruidConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource druidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        return druidDataSource;
    }
}
