package org.ryuuhin.web.config;

import com.fasterxml.classmate.TypeResolver;
import com.google.common.collect.Multimap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.spring.web.plugins.DocumentationPluginsManager;
import springfox.documentation.spring.web.readers.operation.CachingOperationNameGenerator;
import springfox.documentation.spring.web.scanners.ApiDescriptionReader;
import springfox.documentation.spring.web.scanners.ApiListingScanner;
import springfox.documentation.spring.web.scanners.ApiListingScanningContext;
import springfox.documentation.spring.web.scanners.ApiModelReader;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@Configuration
@EnableSwagger2
public class Swagger2 {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.ryuuhin.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot中使用Swagger2构建RESTful APIs")
                .description("更多Spring Boot相关文章请关注：http://blog.didispace.com/")
                .termsOfServiceUrl("http://blog.didispace.com/")
                .contact(new Contact("aa","","aa@email.com"))
                .version("1.0")
                .build();
    }



    public ApiListingScanner addExtraOperations(ApiDescriptionReader apiDescriptionReader,
                                                ApiModelReader apiModelReader,
                                                DocumentationPluginsManager pluginsManager){

        return new FormLoginOperations(apiDescriptionReader, apiModelReader, pluginsManager);
    }


    public class FormLoginOperations extends ApiListingScanner{
        @Autowired
        private TypeResolver typeResolver;

        @Autowired
        public FormLoginOperations(ApiDescriptionReader apiDescriptionReader, ApiModelReader apiModelReader, DocumentationPluginsManager pluginsManager)
        {
            super(apiDescriptionReader, apiModelReader, pluginsManager);
        }

        @Override
        public Multimap<String, ApiListing> scan(ApiListingScanningContext context){
            final Multimap<String, ApiListing> def = super.scan(context);

            final List<ApiDescription> apis = new LinkedList<>();

            final List<Operation> operationsLogin = new ArrayList<>();

            final List<Operation> operationsLogout = new ArrayList<>();

            operationsLogin.add(
                    new OperationBuilder(new CachingOperationNameGenerator())
                    .method(HttpMethod.POST)
                    .uniqueId("login")
                    .parameters(
                            Arrays.asList(
                                    new ParameterBuilder()
                                            .name("username")
                                            .description("用户名")
                                            .parameterType("query")
                                            .type(typeResolver.resolve(String.class))
                                            .modelRef(new ModelRef("string"))
                                            .build(),
                                    new ParameterBuilder()
                                            .name("password")
                                            .description("密码")
                                            .parameterType("query")
                                            .type(typeResolver.resolve(String.class))
                                            .modelRef(new ModelRef("string"))
                                            .build()
                                    )
                    )
                    .summary("登录")
                    .notes("用户登录")
                    .build()
            );

            operationsLogout.add(
                    new OperationBuilder(new CachingOperationNameGenerator())
                    .method(HttpMethod.POST)
                    .uniqueId("logout")
                    .summary("登录")
                    .notes("用户登录")
                    .build()
            );

            apis.add(
                    new ApiDescription(
                            "/api/login_form",
                            "登录",
                            operationsLogin,
                            false)
            );
            apis.add(
                    new ApiDescription(
                            "/api/logout",
                            "注销",
                            operationsLogout,
                            false)
            );

            def.put(
                    "authentication",
                    new ApiListingBuilder(context.getDocumentationContext().getApiDescriptionOrdering())
                            .apis(apis)
                            .description("Custom authentication")
                            .build());
            return def;
        }
    }
}
