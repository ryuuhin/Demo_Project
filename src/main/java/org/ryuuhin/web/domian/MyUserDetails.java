package org.ryuuhin.web.domian;

import org.ryuuhin.ap.domain.SysUserDomain;
import org.ryuuhin.ap.entity.SysRoleEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyUserDetails extends SysUserDomain implements UserDetails {

    public MyUserDetails(SysUserDomain sysUserDomain){
        if (sysUserDomain != null) {
            this.setId(sysUserDomain.getId());
            this.setUsername(sysUserDomain.getUsername());
            this.setAccount(sysUserDomain.getAccount());
            this.setPassword(sysUserDomain.getPassword());
            this.setStatus(sysUserDomain.getStatus());
            this.setAvatar(sysUserDomain.getAvatar());
            this.setBusinessUserId(sysUserDomain.getBusinessUserId());
            this.setEmail(sysUserDomain.getEmail());
            this.setMobile(sysUserDomain.getMobile());
            this.setRoleList(sysUserDomain.getRoleList());
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities(){
        // 将用户角色作为权限
        List<GrantedAuthority> auths = new ArrayList<>();
        List<SysRoleEntity> roles = this.getRoleList();
        for (SysRoleEntity role : roles) {
            auths.add(new SimpleGrantedAuthority(role.getRoleName()));
        }

        return auths;
    }

    /**
     * 是否非过期账户
     * @return
     */
    @Override
    public boolean isAccountNonExpired(){
        return true;
    }

    /**
     * 是否非锁定账户
     * @return
     */
    @Override
    public boolean isAccountNonLocked(){
        return true;
    }

    /**
     * 证书是否过期
     * @return
     */
    @Override
    public boolean isCredentialsNonExpired(){
        return true;
    }

    /**
     * 是否启用
     * @return
     */
    @Override
    public boolean isEnabled(){
        return true;
    }
}
