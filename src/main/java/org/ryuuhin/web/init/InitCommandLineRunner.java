package org.ryuuhin.web.init;

//import com.google.common.base.Enums;
//import com.google.common.base.Strings;
//import com.google.common.collect.Lists;
//import com.useeinfo.jinan.entity.*;
//import com.useeinfo.jinan.enums.MenuTypeEnum;
//import com.useeinfo.jinan.enums.RequestMethodEnum;
//import com.useeinfo.jinan.repository.*;
//import com.useeinfo.jinan.utils.GsonUtil;
//import io.swagger.annotations.ApiOperation;
import org.ryuuhin.ap.entity.SysRoleEntity;
import org.ryuuhin.ap.entity.SysUserEntity;
import org.ryuuhin.ap.repository.SysRoleRepository;
import org.ryuuhin.ap.repository.SysUserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.DefaultApplicationArguments;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author geniusj
 * @email 975473185@qq.com
 * @date 2018/7/5  下午12:55
 */
@Component
public class InitCommandLineRunner implements CommandLineRunner {

    private static final Logger logger  = LoggerFactory.getLogger(InitCommandLineRunner.class);


    @Autowired
    private SysUserRepository sysUserRepository;

    @Autowired
    private SysRoleRepository sysRoleRepository;

    @Autowired
    private WebApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        ApplicationArguments applicationArguments = new DefaultApplicationArguments(args);
        if (applicationArguments.containsOption("init")){
            logger.info("项目启动成功，正在初始化数据...");
            init();
            logger.info("数据初始化成功");
        }
    }

    private void init(){
        initAdminUser();
        initRole();
//        initMenu();
//        initPermission();
//        initAssignPermissions();
    }


    /**
     * 文件转化为String
     * @param file
     * @return
     * @throws IOException
     */
//    private String fileToString(String file) throws IOException {
//        ClassPathResource resource = new ClassPathResource("instructions/" + file + ".json");
//        StringBuilder sb = new StringBuilder("");
//        try(BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()))){
//            String content = null;
//            while((content = br.readLine()) != null){
//                sb.append(content);
//            }
//        }
//        return sb.toString();
//    }

//    private void initMenu(){
//        logger.info("开始初始化菜单...");
//        try {
//            List<SysMenuEntity> sysMenuEntities = Optional.ofNullable(sysMenuRepository.findAll()).orElse(new ArrayList<>());
//            Map<String,Object> map = GsonUtil.JsonToMaps(fileToString("menu"));
//            List<Map<String,Object>> menuMap = (List<Map<String, Object>>) map.get("list");
//            childMenu(null,menuMap,sysMenuEntities);
//        } catch (IOException e) {
//            logger.info("初始化菜单异常...{}",e);
//        }
//        logger.info("初始化菜单结束...");
//    }

//    private void childMenu(SysMenuEntity sysMenuEntity,List<Map<String,Object>> menuMap,List<SysMenuEntity> sysMenuEntities){
//        menuMap.forEach(x->{
//            SysMenuEntity childMenu = new SysMenuEntity();
//            String menuEnName = (String) x.get("enName");
//            Optional<SysMenuEntity> sysMenu = sysMenuEntities.stream()
//                    .filter(f->Optional.ofNullable(f).map(SysMenuEntity::getMenuEnName).orElse("").equals(menuEnName))
//                    .findFirst();
//            if (sysMenu.isPresent()){
//                childMenu = sysMenu.get();
//                childMenu.setUpdateTime(LocalDateTime.now());
//            }else {
//                childMenu.setCreateTime(LocalDateTime.now());
//            }
//            childMenu.setMenuChName((String) x.get("chName"));
//            childMenu.setMenuEnName(menuEnName);
//            childMenu.setRemark((String) x.get("remark"));
//            String type = Optional.ofNullable((String)x.get("type")).orElse("");
//            com.google.common.base.Optional<MenuTypeEnum> menuTypeEnum = Enums.getIfPresent(MenuTypeEnum.class, type);
//            if (menuTypeEnum.isPresent()) {
//                childMenu.setMenuTypeEnum(menuTypeEnum.get());
//            }
//            if (null != sysMenuEntity){
//                childMenu.setParentId(sysMenuEntity.getId());
//            }
//            childMenu = sysMenuRepository.save(childMenu);
//            List<Map<String,Object>> grandChildMenu = (List<Map<String, Object>>) x.get("child");
//            if (!CollectionUtils.isEmpty(grandChildMenu)){
//                childMenu(childMenu,grandChildMenu,sysMenuEntities);
//            }
//        });
//    }

    private void initAdminUser(){
        logger.info("开始初始化超级管理员用户...");
        SysUserEntity sysUserEntity = sysUserRepository.findFirstByAccount("admin");
        if (Objects.isNull(sysUserEntity)){
            sysUserEntity = new SysUserEntity();
            sysUserEntity.setAccount("admin");
            sysUserEntity.setUsername("admin");
            sysUserEntity.setStatus(1);
            sysUserEntity.setPassword(new BCryptPasswordEncoder().encode("admin"));
            sysUserEntity.setCreateTime(LocalDateTime.now());
        }else {
            sysUserEntity.setUpdateTime(LocalDateTime.now());
            sysUserEntity.setStatus(1);
        }
        sysUserRepository.save(sysUserEntity);
        logger.info("初始化超级管理员用户结束...");
    }

    private void initRole(){
        logger.info("开始初始化超级管理员角色...");
        SysRoleEntity sysRoleEntity = sysRoleRepository.findFirstByRoleNameEquals("超级管理员");
        if (Objects.isNull(sysRoleEntity)){
            sysRoleEntity = new SysRoleEntity();
            sysRoleEntity.setRoleName("超级管理员");
            sysRoleEntity.setRemark("超级管理员");
            sysRoleEntity.setStatus(1);
            sysRoleEntity.setCreateTime(LocalDateTime.now());
            sysRoleRepository.save(sysRoleEntity);
        }
        logger.info("初始化超级管理员角色结束...");
    }

//    private void initPermission(){
//        logger.info("开始初始化权限...");
//        RequestMappingHandlerMapping mapping = applicationContext.getBean(RequestMappingHandlerMapping.class);
//        /**
//         * 获取url与类和方法的对应信息
//         */
//        Map<RequestMappingInfo,HandlerMethod> map = mapping.getHandlerMethods();
//        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : map.entrySet()){
//            RequestMappingInfo requestMappingInfo = entry.getKey();
//            Set<String> urls = requestMappingInfo.getPatternsCondition().getPatterns();
//            Set<RequestMethod> methods = requestMappingInfo.getMethodsCondition().getMethods();
//            HandlerMethod handlerMethod = entry.getValue();
//            ApiOperation apiOperation = handlerMethod.getMethodAnnotation(ApiOperation.class);
//            for (String url : urls){
//                if (!Strings.isNullOrEmpty(url)){
//                    if (url.lastIndexOf("/api/")==0 && Objects.nonNull(apiOperation)){
//                        for (RequestMethod method : methods){
//                            SysPermissionEntity sysPermissionEntity = new SysPermissionEntity();
//                            int index =  url.lastIndexOf("/{id}");
//                            if(index>-1){
//                                url = url.substring(0,index);
//                            }
//                            url += "/**";
//                            sysPermissionEntity.setCreateTime(LocalDateTime.now());
//                            sysPermissionEntity.setPermissionUrl(url);
//                            RequestMethodEnum requestMethodEnum = method(method);
//                            sysPermissionEntity.setRequestMethodEnum(requestMethodEnum);
//                            sysPermissionEntity.setPermissionName(apiOperation.value());
//                            String finalUrl = url;
//                            List<SysPermissionEntity> sysPermissionEntityList  = Optional.ofNullable(sysPermissionRepository.findByPermissionUrlAndRequestMethodEnum(finalUrl,requestMethodEnum)).orElse(new ArrayList<>());
//                            if(!CollectionUtils.isEmpty(sysPermissionEntityList)){
//                                sysPermissionEntity.setId(sysPermissionEntityList.get(0).getId());
//                                sysPermissionEntity.setRemark(sysPermissionEntityList.get(0).getRemark());
//                                sysPermissionEntity.setUpdateTime(LocalDateTime.now());
//                            }
//                            sysPermissionRepository.save(sysPermissionEntity);
//                        }
//                    }
//                }
//            }
//        }
//        logger.info("初始化权限结束...");
//    }

//    private RequestMethodEnum method(RequestMethod method){
//        RequestMethodEnum requestMethodEnum = null;
//        if (Objects.nonNull(method)){
//            switch (method){
//                case GET:
//                    requestMethodEnum = RequestMethodEnum.GET;
//                    break;
//                case POST:
//                    requestMethodEnum = RequestMethodEnum.POST;
//                    break;
//                case PUT:
//                    requestMethodEnum = RequestMethodEnum.PUT;
//                    break;
//                case DELETE:
//                    requestMethodEnum = RequestMethodEnum.DELETE;
//                    break;
//                case HEAD:
//                    requestMethodEnum = RequestMethodEnum.HEAD;
//                    break;
//                case PATCH:
//                    requestMethodEnum = RequestMethodEnum.PATCH;
//                    break;
//                case TRACE:
//                    requestMethodEnum = RequestMethodEnum.TRACE;
//                    break;
//                case OPTIONS:
//                    requestMethodEnum = RequestMethodEnum.OPTIONS;
//                    break;
//                default:
//                    requestMethodEnum = RequestMethodEnum.GET;
//                    break;
//            }
//        }
//        return requestMethodEnum;
//    }

//    private void initAssignPermissions(){
//        logger.info("开始初始化权限...");
//        SysUserEntity sysUserEntity = sysUserRepository.findFirstByAccount("admin");
//        SysRoleEntity sysRoleEntity = sysRoleRepository.findFirstByRoleNameEquals("超级管理员");
//        List<SysMenuEntity>  sysMenuEntities= sysMenuRepository.findAll();
//        SysMenuEntity  sysMenu = sysMenuRepository.findFirstByMenuEnNameEquals("ADMIN");
//        List<SysPermissionEntity> sysPermissionEntityList = sysPermissionRepository.findAll();
//        if (Objects.nonNull(sysUserEntity)){
//            SysUserRoleEntity sysUserRoleEntity = sysUserRoleRepository.findFirstByUserId(sysUserEntity.getId());
//            if (Objects.isNull(sysUserRoleEntity)){
//                sysUserRoleEntity = new SysUserRoleEntity();
//            }
//            sysUserRoleEntity.setUserId(sysUserEntity.getId());
//            if (Objects.nonNull(sysRoleEntity)){
//                sysUserRoleEntity.setRoleId(sysRoleEntity.getId());
//                sysUserRoleEntity.setCreateTime(LocalDateTime.now());
//                if (!CollectionUtils.isEmpty(sysMenuEntities)){
//                    List<SysRoleMenuEntity>  findSysRoleMenuEntities = sysRoleMenuRepository.findByRoleId(sysRoleEntity.getId());
//                    List<SysRoleMenuEntity> sysRoleMenuEntities =  new ArrayList<>();
//                    sysMenuEntities.forEach((SysMenuEntity f) ->{
//                        SysRoleMenuEntity sysRoleMenuEntity;
//                        if(!CollectionUtils.isEmpty(findSysRoleMenuEntities)){
//                            Optional<SysRoleMenuEntity> optionalSysRoleMenu = findSysRoleMenuEntities.stream().filter(fi -> fi.getMenuId().equals(f.getId())).findFirst();
//                            if (optionalSysRoleMenu.isPresent()){
//                                sysRoleMenuEntity = optionalSysRoleMenu.get();
//                            }else {
//                                sysRoleMenuEntity =  new SysRoleMenuEntity();
//                            }
//
//                        }else {
//                            sysRoleMenuEntity = new SysRoleMenuEntity();
//                        }
//                        sysRoleMenuEntity.setMenuId(f.getId());
//                        sysRoleMenuEntity.setRoleId(sysRoleEntity.getId());
//                        sysRoleMenuEntity.setCreateTime(LocalDateTime.now());
//                        sysRoleMenuEntities.add(sysRoleMenuEntity);
//                    });
//                    List<SysPermissionEntity> findSysPermission = sysMenuPermissionRepository.findByMenuIds(Lists.newArrayList(sysMenu.getId()));
//                    List<SysMenuPermissionEntity> sysMenuPermissionEntities = new ArrayList<>();
//                    if (!CollectionUtils.isEmpty(sysPermissionEntityList)){
//                        sysPermissionEntityList.forEach(x->{
//                            SysMenuPermissionEntity sysMenuPermissionEntity = new SysMenuPermissionEntity();
//                            if (!CollectionUtils.isEmpty(findSysPermission)){
//                                long count = findSysPermission.stream().filter(fi->fi.getId().equals(x.getId())).count();
//                                if(count<=0L){
//                                    sysMenuPermissionEntity.setMenuId(sysMenu.getId());
//                                    sysMenuPermissionEntity.setPermissionId(x.getId());
//                                    sysMenuPermissionEntity.setCreateTime(LocalDateTime.now());
//                                    sysMenuPermissionEntities.add(sysMenuPermissionEntity);
//                                }
//                            }else {
//
//                                sysMenuPermissionEntity.setMenuId(sysMenu.getId());
//                                sysMenuPermissionEntity.setPermissionId(x.getId());
//                                sysMenuPermissionEntity.setCreateTime(LocalDateTime.now());
//                                sysMenuPermissionEntities.add(sysMenuPermissionEntity);
//                            }
//
//                        });
//                        sysMenuPermissionRepository.save(sysMenuPermissionEntities);
//                    }
//                    sysRoleMenuRepository.save(sysRoleMenuEntities);
//                }
//                sysUserRoleRepository.save(sysUserRoleEntity);
//            }
//        }
//
//        logger.info("初始化权限结束...");
//    }

}
