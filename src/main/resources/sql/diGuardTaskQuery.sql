SELECT
dgt.id,
dt.code,
dgt.apply_for_date,
pi.user_name as 'proposerName',
dgt.proposer_work_unit as 'proposerBankName',
di.user_name as 'dispatcherName',
pui.user_name as 'principalUserName',
dt.type,
sddt.dictionary_name as 'typeName',
dt.scheduled_outset_time,
dt.scheduled_return_time,
dt.status,
sdds.dictionary_name as 'statusName'

FROM
di_guard_task dgt
INNER JOIN di_task dt on dgt.di_task_id = dt.id
LEFT JOIN jinan_apply_application_user_info pi on pi.id = dgt.proposer_id
LEFT JOIN jinan_apply_application_user_info di on di.id = dt.dispatcher_id
LEFT JOIN jinan_apply_application_user_info pui on pui.id = dt.principal_user_id
LEFT JOIN sys_data_dictionary sddt on sddt.dictionary_code = dt.type and sddt.type_code = 'TASK_TYPE'
LEFT JOIN sys_data_dictionary sdds on sdds.type_code = 'ALLDAY_TASK_STATUS' and sdds.dictionary_code = dt.status
where 1 = 1
<@condition>
order by
(case when dt.status <> 'ALLDAY_TASK_STATUS_FINISH' then 'actual_return_time asc' ELSE 'actual_return_time desc' end),
dt.update_time desc